const express = require('express');
const path = require('path');
const mongoose = require('mongoose');
const expressLayouts = require('express-ejs-layouts');

const app = express();

// Conection MongoDB
mongoose
	.connect(
		`mongodb://${process.env.MONGO_DB_USERNAME}:${process.env.MONGO_DB_PWD}@mongodb`,
		{
			useNewUrlParser: true,
			useUnifiedTopology: true,
			dbName: 'students_api',
		}
	)
	.then(() => console.log('MongoDB is Connected...'))
	.catch((err) => console.log(err));

// Morgan middleware
// app.use(morgan('dev'));

// Express Bodyparser
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Static Files
app.use('/bootstrap', express.static(path.join(__dirname, 'bootstrap')));
// Set Templating Engine
app.use(expressLayouts);
app.set('layout', './layouts/main');
app.set('view engine', 'ejs');

app.use('/', require('./routes/index'));

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Server running on ${port}...`));
