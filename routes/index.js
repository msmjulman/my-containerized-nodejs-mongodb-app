const express = require('express');
const Student = require('../models/Student');
const router = express.Router();

router.get('/', (req, res) => {
	res.render('home');
});

router.post('/', (req, res) => {
	const { nom, email, age } = req.body;

	const newStudent = new Student({
		nom,
		email,
		age,
	});

	newStudent
		.save()
		.then(() => {
			res.status(200).redirect('/');
		})
		.catch((err) => {
			console.log(err);
		});
});

router.get('/view', (req, res) => {
	Student.find()
		.then((students) => {
			res.render('liste-etudiant', { students });
		})
		.catch((err) => console.log(err));
});

module.exports = router;
