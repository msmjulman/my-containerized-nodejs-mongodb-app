const mongoose = require('mongoose');

const StudentSchema = mongoose.Schema({
	nom: {
		type: String,
		required: true,
	},
	email: {
		type: String,
		required: true,
	},
	age: {
		type: Number,
		required: true,
	},
	created_at: {
		type: Date,
		default: Date.now,
	},
});

module.exports = mongoose.model('Students', StudentSchema);
