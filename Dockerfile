FROM node:12.20-alpine

ENV MONGO_DB_USERNAME=julman \
    MONGO_DB_PWD=julio_secret

WORKDIR /home/app

ADD package*.json ./

RUN npm install

ADD . .

CMD ["node","app.js"]